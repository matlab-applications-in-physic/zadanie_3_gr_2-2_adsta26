/*
Matlab applications in physics
Author: Adam Stasiak
Engineering Physics
*/

//Please ensure that the data to be analyzed is in the 'data' folder, which is in the folder with the script 'analizaPM.sce'
//File with results ('PMresults.dat') will be created in folder, where is the script 'analizaPM.sce'

clear //clearing memory

DUSK_TIME = 18 //estimated dusk hour in October
DAWN_TIME = 7  //estimated dawn hour in October

path = get_absolute_file_path('analizaPM.sce') + '/data/' //path to the folder, where should be data to analyze

dataFiles = listfiles(path) //list of data files names
dataFiles = gsort(dataFiles, 'lr', 'i') //sorted list of data file names by date

data = list() //empty list for reading data from files

for i = 1:size(dataFiles)(1) //loop running as many times as there were measuring days
    data(i)  = csvRead(path + dataFiles(i), ';', ',', 'string') //reading data from a file as text
    data(i)(26:28,:) = [] //deleting unnecessary data
    data(i)(1,:) = []
    data(i)(:,2:5) = []
        for j = 1:size(data(i))(1) //loop running as many times as the measured data on the day
            data(i)(j,1) = strsplit(dataFiles(i),['_';'.'])(2) + ' ' + data(i)(j,1) //adding the date read from the file name to the cell containing time
        end
    days = i //Number of days on which measurements were made
end

dataAllTogether = cat(1,data(1:days)) //data connection for individual days

pm10Max = max(strtod(dataAllTogether(:,2))) //maximum concentration of pm10
pm10MaxIndexes = find(dataAllTogether(:,2)==string(pm10Max)) //finding all indexes containing max concentration pm10

counterDay = 1 //counter for day measurements 
counterNight = 1 //counter for night measurements 
for i = 1:days //loop running as many times as there were measuring days
    for j = 1:size(data(i))(1) //loop running as many times as the measured data on the day
        temp = strtod( strsplit(data(i)(j,1), [' ', ':'])(2) ) //reading the hour from a cell containing date and time
        if temp > DAWN_TIME && temp < DUSK_TIME
            pm10DawnToDusk(counterDay) = strtod(data(i)(j,2)) //if the hour read is between dawn and dusk, add the measurement to the list containing the measurements of the day
            counterDay = counterDay + 1 //increase the counter
        else
            pm10DuskToDawn(counterNight) = strtod(data(i)(j,2)) //if the hour read is between dusk and dawn, add the measurement to the list containing the measurements of the night
            counterNight = counterNight + 1 //increase the counter
        end
    end
end

dailyAveragePm10 = mean(pm10DawnToDusk) //average pm10 concentration at day
nightAveragePm10 = mean(pm10DuskToDawn) //average pm10 concentration at night

dayUncertainty = stdev(pm10DawnToDusk)/size(pm10DawnToDusk)(1) //statistical uncertainty of daytime measurements
nightUncertainty = stdev(pm10DuskToDawn)/size(pm10DuskToDawn)(1) //statistical uncertainty of measurements taken at night

complianceTest = abs(dailyAveragePm10-nightAveragePm10)/sqrt(dayUncertainty^2 + nightUncertainty^2) //compliance test result

outputFile = mopen(get_absolute_file_path('analizaPM.sce') + 'PMresults.dat', 'wt') //opening the file with analysis results

//saving the maximum concentration of pm10 to the results file along with the measurement time and display this informations on the screen
mfprintf(outputFile, 'The maximum concentration of pm10 in the analyzed period (' + dataAllTogether(1,1) + ' to ' + dataAllTogether(size(dataAllTogether)(1),1) + ') was:\n')
printf('The maximum concentration of pm10 in the analyzed period (' + dataAllTogether(1,1) + ' to ' + dataAllTogether(size(dataAllTogether)(1),1) + ') was:\n')

if length(pm10MaxIndexes)(1) > 1 
    //if the maximum concentration value pm10 is repeated then display this value together with the times corresponding to it
    mfprintf(outputFile, string(pm10Max) + ' µg/m3' + ', which was recorded on: ')
    printf(string(pm10Max) + ' µg/m3' + ', which was recorded on: ')
    for i = 1:length(pm10MaxIndexes)(1) - 1 //loop running one times less than the number of repetitions of the maximum concentration of pm10
        //saving and displaying dates and times separated by commas
        mfprintf(outputFile, dataAllTogether(pm10MaxIndexes(i),1) + ', ')
        printf(dataAllTogether(pm10MaxIndexes(i),1) + ', ')
    end
    //saving and displaying the last date and time
    mfprintf(outputFile, dataAllTogether(pm10MaxIndexes(length(pm10MaxIndexes)(1)),1) + '\n\n')
    printf(dataAllTogether(pm10MaxIndexes(length(pm10MaxIndexes)(1)),1) + '\n\n')
else
    //if the maximum concentration value pm10 does not repeat, display this value along with the measurement time
    mfprintf(outputFile, string(pm10Max) + 'µg/m3' + ', which was recorded on ' + dataAllTogether(pm10MaxIndexes,1) + '\n\n')
    printf(string(pm10Max) + 'µg/m3' + ', which was recorded on ' + dataAllTogether(pm10MaxIndexes,1) + '\n\n')
end

//saving the interpretation of the compliance test to a file and displaying it on the screen
if complianceTest <= 1
    mfprintf(outputFile, 'The compliance test results (' + string(complianceTest))
    mfprintf(outputFile, ') show that the pm10 concentration readings between day and night statistically do not differ from each other.')
    printf('The compliance test results (' + string(complianceTest))
    printf(') show that the pm10 concentration readings between day and night statistically do not differ from each other.')
elseif complianceTest > 1 && complianceTest <= 3 
    mfprintf(outputFile, 'The compliance test results (' + string(complianceTest))
    mfprintf(outputFile, ') show that it cannot be excluded that pm10 readings between day and night do not statistically differ from each other.')
    printf('The compliance test results (' + string(complianceTest))
    printf(') show that it cannot be excluded that pm10 readings between day and night do not statistically differ from each other.')
else
    mfprintf(outputFile, 'The compliance test results (' + string(complianceTest))
    mfprintf(outputFile, ') show that the pm10 concentration readings between day and night statistically differ from each other.')
    printf('The compliance test results (' + string(complianceTest))
    printf(') show that the pm10 concentration readings between day and night statistically differ from each other.')
end

mclose('all') //closing the file with analysis results
clear //clearing memory
